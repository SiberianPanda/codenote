#!/bin/bash
if ! test -d ./log/entrypoint/; then
  mkdir -p ./log/entrypoint/
fi
if test -f ./tmp/pids/server.pid; then
  rm ./tmp/pids/server.pid 2>/dev/null 1>/dev/null
fi
if [[ $FIRST -eq 1 ]]; then
  bundle install
  rails db:create
  rails db:migrate
fi
bundle 2>./log/entrypoint/bundle_log_errors 1>./log/entrypoint/bundle_log
rails s