#!/bin/bash
export FIRST=0
case $1 in
  help)
    echo -e `cat description_server.txt`
    ;;
  db_setup)
    docker exec -it note_server rails db:create &&\
    docker exec -it note_server rails db:migrate &&\
    docker exec -it note_server rails db:seed
    ;;
  rails)
    case $2 in
      -c)
        docker exec -it note_server rails c
        docker exec -it note_server chmod -R 777 .
        ;;
      *)
        shift
        docker exec -it note_server rails $@
        docker exec -it note_server chmod -R 777 .
        ;;
    esac
    ;;
  seed)
    docker exec -it note_server rails db:seed
    ;;
  migrate)
    # проверяем наличие базы и  если её не существует, создаем
    # если существует проводим миграции
    docker exec -it note_server rails db:exists && docker exec -it note_server rails db:migrate || docker exec -it note_server rails db:setup
    ;;
  db_reset)
    docker exec -it note_server rails db:drop && docker exec -it note_server rails db:create && docker exec -it note_server rails db:migrate
    ;;
  bash)
    case $2 in
      -c)
        docker exec -it note_server /bin/bash
        docker exec -it note_server chmod -R 777 .
        ;;
      *)
        shift
        docker exec -it note_server /bin/bash $@
        docker exec -it note_server chmod -R 777 .
        ;;
    esac
    ;;
  stop)
    docker stop note_database
    docker stop note_server
    docker stop note_frontend
    ;;
  first_start)
    export FIRST=1
    case $2 in
      -d)
        echo -e "\033[1;32m\033[46m DEVELOPMENT SERVER STARTING AS DAEMON\033[0m"
        docker-compose -f docker-compose.yml up -d --build 
        docker exec -it note_server chmod -R 777 .
        ;;
      *)
        echo -e "\033[1;30m\033[46m DEVELOPMENT SERVER STARTING \033[0m"
        docker-compose -f docker-compose.yml  up --build 
        docker exec -it note_server chmod -R 777 .
        ;;
    esac
    ;;
  *)
    case $2 in
      -d)
        echo -e "\033[1;32m\033[46m DEVELOPMENT SERVER STARTING AS DAEMON\033[0m"
        docker-compose -f docker-compose.yml up -d --build 
        docker exec -it note_server chmod -R 777 .
        ;;
      *)
        echo -e "\033[1;30m\033[46m DEVELOPMENT SERVER STARTING \033[0m"
        docker-compose -f docker-compose.yml  up --build 
        docker exec -it note_server chmod -R 777 .
        ;;
    esac
    ;;
esac