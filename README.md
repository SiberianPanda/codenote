# Code note

## start script

### start server  
  
`./start.sh` - **start** development **server**
  
`./start.sh -d` - **start** dev **server as daemon**
  
`./start.sh first_start` - start dev server and **before start** this **setup your database**
  
### options when server started  
  
`./start.sh bash -c` - **enter in rails container** with bash  

`./start.sh rails -c` - **enter in rails console**

`./start.sh rails [command]` try **exec command** with rails (example: `./start.sh rails db:seed`)

